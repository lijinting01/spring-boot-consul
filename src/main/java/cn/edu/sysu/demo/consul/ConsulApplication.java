package cn.edu.sysu.demo.consul;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

@Slf4j
@EnableDiscoveryClient
@EnableConfigurationProperties
@SpringBootApplication
public class ConsulApplication {

  @Bean
  CommandLineRunner checkJdbc(JdbcTemplate jdbcTemplate) {
    return args -> log.info(jdbcTemplate.queryForObject("SELECT USER()", String.class));
  }

  public static void main(String[] args) {
    SpringApplication.run(ConsulApplication.class, args);
  }

}
