package cn.edu.sysu.demo.consul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class HomeController {

  private Locale locale;

  public HomeController(@Autowired Locale locale) {
    this.locale = locale;
  }

  @GetMapping
  public String home() {
    return "Welcome home : " + locale;
  }
}
