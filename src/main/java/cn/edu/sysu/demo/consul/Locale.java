package cn.edu.sysu.demo.consul;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "locale")
public class Locale {

  private String country;

  private String state;

  private String city;
}
